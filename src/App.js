import React, { useState } from 'react';
import './App.css';

function App() {
  const [conversion, setConversion] = useState({ Fahrenheit: 0, Celcius: 0 })

  const validate = (entry) => {
    const numeric = /^[0-9\b]+$/;
    if (entry.target.value === '' || numeric.test(entry.target.value)) {
      return true
    }

  }

  const setCelcius = entry => {
    if (validate(entry)) {
      setConversion({
        Celcius: entry.target.value,
        Fahrenheit: (entry.target.value * 9/5 + 32).toFixed(2)
      })
    }
    
  }

  const setFahrenheit = entry => {
    if (validate(entry)) {
      setConversion({
        Celcius: ((entry.target.value - 32) * 5/9).toFixed(2),
        Fahrenheit: entry.target.value 
      })
    }
  }

  return (
    <div className="Direction">
      <div className="Container">
        <input className="Left" value={conversion.Celcius} onChange={setCelcius}/>
        <div className="Right">&#176;C</div>
      </div>
      <div className="Container">
        <input className="Left" value={conversion.Fahrenheit} onChange={setFahrenheit}/>
        <div className="Right">&#176;F</div>
      </div>
    </div>
  );
}

export default App;